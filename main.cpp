#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include "game.h"
#include "our_player.h"
int main(int argc, char **argv) {
    srand(SEED);
    char buf[32];

    Player *player = new OurPlayer(15);

    while (true) {
        scanf("%s", buf);
        if (!strcmp(buf, "boardsize")) {
            int w, h;
            scanf("%d %d", &w, &h);
            printf("=\n\n");
            player->boardsize(w + 2, h + 2);
        } else if (!strcmp(buf, "play")) {
            int d;
            scanf("%d", &d);
            printf("=\n\n");
            player->play(d);
        } else if (!strcmp(buf, "genmove")) {
            printf("= %d\n\n", player->genmove());
        } else if (!strcmp(buf, "timeleft")) {
            int t;
            scanf("%d", &t);
            printf("=\n\n");
            player->timeleft(t);
        }
        fflush(stdout);
    }
    return 0;
}
