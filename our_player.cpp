#include "our_player.h"
#include <cstdlib>
#include <cassert>
#include <algorithm>
using namespace std;

OurPlayer::OurPlayer(unsigned int tt_size)
{
    tt = new TranspositionTable(tt_size);
    game = NULL;
}

OurPlayer::~OurPlayer()
{
    if (game != NULL)
        delete game;
    delete tt;
}

int OurPlayer::alphabeta(Game *node, int depth, int alpha, int beta, int who)
{
    unsigned long long ha = node->hash;
    int hashf = hashfALPHA;
    int val;
    int out_depth;
    int out_move = -1;
    if ((val = tt->get(node->hash, depth, alpha, beta, &out_depth, &out_move)) != ENTRY_NOT_FOUND)
        if (out_depth >= depth)
            return val;

    int node_score = node->score() * who;
    if (node_score == 100)
        return node_score + depth;
    if (node_score == -100)
        return node_score - depth;

    if (depth == 0)
    {
        if (node->move_finished())
            return node_score;
        else
            return SCORE_UNKNOWN;
    }

    char moves[9];
    gen_all_moves(moves);
    for (int i = 1; i < moves[0]; i++)
        if (moves[1+i] == out_move)
        {
            char p = moves[1];
            moves[1] = moves[1+i];
            moves[1+i] = p;
            break;
        }
    bool good_score = false;
    int best_move = -1;
    for (int i = 0; i < moves[0]; i++)
    {
        char move = moves[1 + i];
        bool my = node->my_move;
        node->move(move);
        int score;
        if (my == node->my_move)
        {
            score = alphabeta(node, depth - 1, alpha, alpha+1, who);
            if (alpha < score && score < beta)
                score = alphabeta(node, depth - 1, score, beta, who);
        }
        else
        {
            score = -alphabeta(node, depth - 1, -alpha-1, -alpha, -who);
            if (alpha < score && score < beta)
                score = -alphabeta(node, depth - 1, -beta, -score, -who);
        }
        node->undo(move);
        assert(node->hash == ha);
        if (score == SCORE_UNKNOWN || -score == SCORE_UNKNOWN)
            continue;
        else
            good_score = true;

        if (score > alpha)
        {
            alpha = score;
            hashf = hashfEXACT;
            best_move = move;
        }
        if (alpha >= beta)
        {
            tt->insert(node->hash, depth, beta, hashfBETA, move);
            return alpha;
        }
    }
    if (!good_score && node->move_finished())
    {
        tt->insert(node->hash, depth, node_score, hashf, -1);
        return node_score;
    }

    tt->insert(node->hash, depth, alpha, hashf, best_move);
    return alpha;
}

int OurPlayer::alphabeta_root(Game *node, int depth, int *out_score)
{
    int alpha = -120;
    int beta = 120;
    int best_move = 0;
    char moves[9];
    
    int out_depth;
    int out_move = -1;
    tt->get(node->hash, depth, alpha, beta, &out_depth, &out_move);

    node->my_move = true;
    gen_all_moves(moves);

    for (int i = 1; i < moves[0]; i++)
        if (moves[1+i] == out_move)
        {
            char p = moves[1];
            moves[1] = moves[1+i];
            moves[1+i] = p;
            break;
        }
    *out_score = SCORE_UNKNOWN;
    for (int i = 0; i < moves[0]; i++)
    {
        char move = moves[1 + i];
        int score;

        node->move(move);
        if (node->my_move)
            score = alphabeta(node, depth - 1, alpha, beta, 1);
        else
            score = -alphabeta(node, depth - 1, -beta, -alpha, -1);
        node->undo(move);

        if (score == SCORE_UNKNOWN || -score == SCORE_UNKNOWN)
            continue;

        if (score > alpha)
        {
            alpha = score;
            best_move = move;
        }

        if (alpha >= beta)
        {
            *out_score = alpha;
            return best_move;
        }
    }
    *out_score = alpha;
    return best_move;
}

int OurPlayer::genmove()
{
    int best_move;
    int score;
    int depth = 17;
    if (time_left < 4*60*1000)
        depth = 15;
    if (time_left < 60*1000)
        depth = 13;
    for (int i = 1; i <= depth; i++)
    {
        best_move = alphabeta_root(game, i, &score);
    }

    game->move(best_move);
    game->edges++;
    return best_move;
}

void OurPlayer::play(int m)
{
    game->move(m);
    game->print_board();
    game->edges++;
}

void OurPlayer::timeleft(int t)
{
    time_left = t;
}

void OurPlayer::boardsize(int w, int h)
{
    game = new Game(w, h);
    game->init_board();
}

void OurPlayer::gen_all_moves(char moves[9])
{
    char m = ~game->segs[game->ballpos];
    int cnt = 0;
    for (int i = 0; i < 8; i++)
        if (m & (1<<i))
            moves[1 + cnt++] = i;
    moves[0] = cnt;
}
