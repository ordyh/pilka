#ifndef GAME_H
#define GAME_H

#define SCORE_UNKNOWN 1000

enum class Field {
    EMPTY, WALL, GOAL1, GOAL2
};

enum Segment {
    RIGHT =     1<<0,
    UPRIGHT =   1<<1,
    UP =        1<<2,
    UPLEFT =    1<<3,
    LEFT =      1<<4,
    DOWNLEFT =  1<<5,
    DOWN =      1<<6,
    DOWNRIGHT = 1<<7
};

class Game {
    public:
        Game(int width, int height);
        
        int pos_to_row(int pos);
        int pos_to_col(int pos);
        int rc_to_pos(int row, int col);
        void init_board();
        bool is_wall(int pos);
        char field_to_char(Field f);
        void print_board();
        void mark(int pos, char segs_set);
        void unmark(int pos, char segs_set);

        void move(int m);
        void undo(int m);
//    private:
        const static int MAXCOLS = 8;
        const static int MAXROWS = 10;
        int cols;
        int rows;
        int edges;
        int mv[8];
        int ballpos;
        unsigned long long hash;
        unsigned long long hashes[(MAXCOLS + 3)*(MAXROWS + 5)][8];
        unsigned char segs[(MAXCOLS + 3)*(MAXROWS + 5)];
        Field board[(MAXCOLS + 3)*(MAXROWS + 5)];
        int score();
        bool move_finished();
        bool my_move;
};

#endif
