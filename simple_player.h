#ifndef SIMPLE_PLAYER_H
#define SIMPLE_PLAYER_H

#include "player.h"

class SimplePlayer: public Player {
public:
    int genmove();
    void play(int move);
    void timeleft(int t);
    void boardsize(int w, int h);
};

#endif
