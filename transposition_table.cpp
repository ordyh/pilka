#include "transposition_table.h"
#include <cassert>
#include <cstdio>
TranspositionTable::TranspositionTable(int size):
    size(size)
{
    table = new TTCell[(1 << size) + 5];
}

TranspositionTable::~TranspositionTable()
{
    delete table;
}

void TranspositionTable::insert(unsigned long long hash, int depth, int val, int type, int move)
{
    if (depth <= 4)
        return;

    unsigned int idx = hash % (1 << size);
    TTCell *cell = &table[idx];

    for (int i = 0; i < 4; i++, cell++)
    {
        if (cell->hash == hash)
        {
            if (cell->depth < depth)
            {
                cell->val = val;
                cell->type = type;
                cell->depth = depth;
                cell->move = move;
            }
            return;
        }
    }

    cell = &table[idx];

    for (int i = 0; i < 4; i++, cell++)
    {
        if (cell->depth < depth)
        {
            cell->hash = hash;
            cell->depth = depth;
            cell->type = type;
            cell->val = val;
            cell->move = move;
            return;
        }
    }
}

int TranspositionTable::get(unsigned long long hash, int depth, int alpha, int beta, int *out_depth, int *out_move)
{
    unsigned int idx = hash % (1 << size);
    TTCell *cell = &table[idx];

    for (int i = 0; i < 4; i++, cell++)
        if (cell->hash == hash)
        {
            *out_depth = cell->depth;
            *out_move = cell->move;
            if (cell->type == hashfEXACT)
                return cell->val;
            if (cell->type == hashfALPHA && cell->val <= alpha)
                return alpha;
            if (cell->type == hashfBETA && cell->val >= beta)
                return beta;

            break;
        }

    return ENTRY_NOT_FOUND;
}
