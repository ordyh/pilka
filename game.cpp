#include "game.h"
#include <cstdio>
#include <cstdlib>
Game::Game(int cols, int rows):
    cols(cols + 1), rows(rows + 3)
{
    hash = 0;
    edges = 0;
    for (int i = 0; i < this->cols * this->rows; i++)
        for (int j = 0; j < 8; j++)
            hashes[i][j] = rand();
}

int Game::pos_to_row(int pos)
{
    return pos / cols;
}

int Game::pos_to_col(int pos)
{
    return pos % rows;
}

int Game::rc_to_pos(int row, int col)
{
    return row * cols + col;
}

void Game::init_board()
{

    ballpos = rc_to_pos(rows/2, cols/2);

    mv[0] = 1;
    mv[1] = -cols + 1;
    mv[2] = -cols;
    mv[3] = -cols - 1;
    mv[4] = -1;
    mv[5] = cols - 1;
    mv[6] = cols;
    mv[7] = cols + 1;

    for (int pos = 0; pos < rows*cols; pos++)
        board[pos] = Field::WALL;

    for (int row = 3; row <= rows - 4; row++)
        for (int col = 2; col <= cols - 3; col++)
            board[rc_to_pos(row, col)] = Field::EMPTY;
    
    board[rc_to_pos(2, cols/2)] = Field::EMPTY;
    board[rc_to_pos(rows - 3, cols/2)] = Field::EMPTY;

    for (int col = 1; col <= cols - 2; col++)
        if (col != cols/2) {
            board[rc_to_pos(2, col)] = Field::WALL;
            board[rc_to_pos(rows - 3, col)] = Field::WALL;
        }

    for (int row = 2; row <= rows - 3; row++) {
        board[rc_to_pos(row, 1)] = Field::WALL;
        board[rc_to_pos(row, cols - 2)] = Field::WALL;
    }
    
    for (int col = cols/2 - 1; col <= cols/2 + 1; col++) {
        board[rc_to_pos(1, col)] = Field::GOAL1;
        board[rc_to_pos(rows - 2, col)] = Field::GOAL2;
    }


    for (int pos = 0; pos < rows*cols; pos++)
        segs[pos] = 0;

    for (int col = 1; col <= cols - 2; col++) {
        if (col < cols/2 - 1 || col > cols/2 + 1) {
            mark(rc_to_pos(1, col), 0xFF);
            mark(rc_to_pos(rows - 2, col), 0xFF);
        } else if (col == cols/2 - 1) {
            mark(rc_to_pos(1, col), 0xFF & ~(DOWNRIGHT));
            mark(rc_to_pos(rows - 2, col), 0xFF & ~(UPRIGHT));
        } else if (col == cols/2) {
            mark(rc_to_pos(1, col), 0xFF & ~(DOWNLEFT | DOWN | DOWNRIGHT));
            mark(rc_to_pos(rows - 2, col), 0xFF & ~(UPLEFT | UP | UPRIGHT));
        } else if (col == cols/2 + 1) {
            mark(rc_to_pos(1, col), 0xFF & ~(DOWNLEFT));
            mark(rc_to_pos(rows - 2, col), 0xFF & ~(UPLEFT));
        }
    }

    for (int col = 1; col <= cols - 2; col++) {
        if (col < cols/2) {
            mark(rc_to_pos(2, col), LEFT);
            mark(rc_to_pos(rows - 3, col), LEFT);
        } else if (col > cols/2) {
            mark(rc_to_pos(2, col), RIGHT);
            mark(rc_to_pos(rows - 3, col), RIGHT);
        }
    }
    
    for (int row = 2; row <= rows - 3; row++) {
        mark(rc_to_pos(row, 1), 0xFF & ~(UPRIGHT | RIGHT | DOWNRIGHT));
        mark(rc_to_pos(row, cols - 2), 0xFF & ~(UPLEFT | LEFT | DOWNLEFT));
    }
}

void Game::mark(int pos, char segs_set)
{
    for (int i = 0; i < 8; i++) {
        if (segs_set & (1 << i)) {
            if (!(segs[pos + mv[i]] & (1 << (i ^ 4))))
            {
                segs[pos + mv[i]] |= 1 << (i ^ 4);
                hash ^= hashes[pos + mv[i]][i^4];
                hash ^= hashes[pos][i];
            }
        }
    }

    segs[pos] |= segs_set;
}

void Game::unmark(int pos, char segs_set)
{
    for (int i = 0; i < 8; i++) {
        if (segs_set & (1 << i)) {
            if (segs[pos + mv[i]] & (1 << (i ^ 4)))
            {
                segs[pos + mv[i]] &= 0xFF & (~(1 << (i ^ 4)));
                hash ^= hashes[pos + mv[i]][i ^ 4];
                hash ^= hashes[pos][i];
            }
        }
    }

    segs[pos] &= 0xFF & (~segs_set);
}

bool Game::is_wall(int pos)
{
    return true;
}

char Game::field_to_char(Field f)
{
    switch (f) {
        case Field::EMPTY:
            return ' ';
        case Field::WALL:
            return 'X';
        case Field::GOAL1:
        case Field::GOAL2:
            return '.';
        default:
            return '?';
    }
}

void Game::print_board()
{
    for (int i = 0; i < 2 * rows - 1; i++) {
        for (int j = 0; j < 2 * cols - 1; j++) {
            if (i%2 == 0) {
                if (j%2 == 0) {
                    if (ballpos == rc_to_pos(i/2, j/2))
                        fprintf(stderr, "o");
                    else
                    {
                        if (segs[rc_to_pos(i/2, j/2)] != 0)
                            fprintf(stderr, ".");
                        else
                            fprintf(stderr, " ");
                    }
                } else {
                    if (segs[rc_to_pos(i/2, j/2)] & RIGHT)
                        fprintf(stderr, "-");
                    else
                        fprintf(stderr, " ");
                }
            } else {
                if (j % 2 == 0) {
                    if (segs[rc_to_pos(i/2, j/2)] & DOWN)
                        fprintf(stderr, "|");
                    else
                        fprintf(stderr, " ");
                } else {
                    bool a = segs[rc_to_pos(i/2, j/2)] & DOWNRIGHT;
                    bool b = segs[rc_to_pos(i/2, j/2 + 1)] & DOWNLEFT;
                    if (a && b) 
                        fprintf(stderr, "X");
                    else if (a)
                        fprintf(stderr, "\\");
                    else if (b)
                        fprintf(stderr, "/");
                    else
                        fprintf(stderr, " ");
                }
            }
        }
        fprintf(stderr, "\n");
    }
    /*for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++)
            putchar(field_to_char(board[rc_to_pos(i, j)]));
        putchar('\n');
    }*/
}

void Game::move(int m)
{
    mark(ballpos, 1<<m);
    ballpos += mv[m];
    
    if (segs[ballpos] == 1<<(m^4))
    {
        my_move = !my_move;
        hash ^= 1;
    }

}

void Game::undo(int m)
{
    if (segs[ballpos] == 1<<(m^4))
    {
        my_move = !my_move;
        hash ^= 1;
    }
    ballpos -= mv[m];
    unmark(ballpos, 1<<m);
}

int Game::score()
{
    int row = pos_to_row(ballpos);
    if (row == rows - 2)
        return -100;
    if (row == 1)
        return 100;

    if (segs[ballpos] == 0xFF)
    {
        if (my_move)
            return -100;
        else
            return 100;
    }
    return rows/2 - row; 
}

bool Game::move_finished()
{
    int cnt = 0;
    for (int i = 0; i < 8 && cnt < 2; i++)
        if (segs[ballpos] & (1<<i))
            cnt++;

    return cnt == 1;
}
