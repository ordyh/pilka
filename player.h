#ifndef PLAYER_H
#define PLAYER_H

class Player {
public:
    virtual int genmove() = 0;
    virtual void play(int move) = 0;
    virtual void timeleft(int t) = 0;
    virtual void boardsize(int w, int h) = 0;
};

#endif

