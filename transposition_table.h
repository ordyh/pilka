#ifndef TRANSPOSITION_TABLE_H
#define TRANSPOSITION_TABLE_H

#define hashfEXACT 0
#define hashfALPHA 1
#define hashfBETA 2

#define ENTRY_NOT_FOUND 1000

struct TTCell {
    unsigned long long hash;
    int val;
    int move;
    int type;
    int depth;
    TTCell()
    {
        depth = -1;
    }
};

class TranspositionTable {
    public:
        TranspositionTable(int size);
        ~TranspositionTable();

        int size;
        TTCell *table;
        void insert(unsigned long long hash, int depth, int val, int type, int move);
        int get(unsigned long long hash, int depth, int alpha, int beta, int *out_depth, int *out_move);

        unsigned long long good, bad;
        unsigned long long cnt;
};

#endif
