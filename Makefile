all: player
FLAGS= -O2 -Wall --std=c++11 -DSEED=123
SOURCES=game.cpp simple_player.cpp our_player.cpp main.cpp transposition_table.cpp
OBJS=$(SOURCES:.cpp=.o)

player: $(OBJS)
	g++ $(FLAGS) $(OBJS) -o player

main.o: main.cpp
	g++ $(FLAGS) -c $<

%.o: %.cpp %.h
	g++ $(FLAGS) -c $<

clean:
	rm -rf *.o

