#ifndef OUR_PLAYER_H
#define OUR_PLAYER_H

#include "simple_player.h"
#include "game.h"
#include "transposition_table.h"
#include <algorithm>
using namespace std;
class OurPlayer: public SimplePlayer {
public:
    OurPlayer(unsigned int tt_size);
    ~OurPlayer();
    int genmove();
    void play(int move);
    void timeleft(int t);
    void boardsize(int w, int h);

private:
    Game *game;
    TranspositionTable *tt;
    int time_left;
    void gen_all_moves(char moves[9]);
    int alphabeta(Game *node, int depth, int alpha, int beta, int who);
    int alphabeta_root(Game *node, int depth, int *out_score);
};

#endif

